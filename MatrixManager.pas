unit MatrixManager;

{$mode objfpc}{$H+}

interface
const
     MaxElement = 15;
     MinElement = -15;
     MatrixSize = 10;
     MatrixAmount = MaxElement - MinElement + 1;
type
     MatrixSquare = array[1..MatrixSize, 1..MatrixSize] of Integer;

procedure FillMatrix(var matrix: MatrixSquare);
procedure PrintMatrix(matrix: MatrixSquare);
function GetCountNegativeElement(matrix: MatrixSquare): Integer;
procedure ReplaceOnZero(var matrix: MatrixSquare);
function GetMinMaxDifference(matrix: MatrixSquare): Integer;
procedure ReplaceOnOne(var matrix: MatrixSquare);

implementation

    function GetMaximum(matrix: MatrixSquare): Integer;
    var
        i, j, temp: Integer;
    begin
        temp := matrix[1, 1];
        for i := 1 to MatrixSize do
        begin
            for j := 1 to MatrixSize do
            begin
                if matrix[i, j] > temp then
                begin
                    temp := matrix[i, j];
                end;
            end;
        end;
        GetMaximum := temp;
    end;

    function GetMinimum(matrix: MatrixSquare): Integer;
    var
        i, j, temp: Integer;
    begin
        temp := matrix[1, 1];
        for i := 1 to MatrixSize do
        begin
            for j := 1 to MatrixSize do
            begin
                if matrix[i, j] < temp then
                begin
                    temp := matrix[i, j];
                end;
            end;
        end;
        GetMinimum := temp;
    end;

    function GetMinMaxDifference(matrix: MatrixSquare): Integer;
    var
        count: Integer;
    begin
        count := GetMaximum(matrix) - GetMinimum(matrix);
    end;

    procedure FillMatrix(var matrix: MatrixSquare);
    var
        i, j: Integer;
    begin
        for i := 1 to MatrixSize do
        begin
            for j := 1 to MatrixSize do
            begin
                matrix[i, j] := random(MatrixAmount) + MinElement;
            end;
        end;
    end;

    procedure PrintMatrix(matrix: MatrixSquare);
    var
        i, j: Integer;
    begin
        for i := 1 to MatrixSize do
        begin
            for j := 1 to MatrixSize do
            begin
                Write(matrix[i, j]:4);
            end;
            WriteLn;
            WriteLn;
       end;
    end;

    function GetCountNegativeElement(matrix: MatrixSquare): Integer;
    var
        i, count: Integer;
    begin
        count := 0;
        for i := 1 to MatrixSize do
        begin
            if matrix[i, i] < 0 then
            begin
                Inc(count);
            end;
        end;
        GetCountNegativeElement := count;
    end;

    procedure ReplaceOnZero(var matrix: MatrixSquare);
    var
        i, j: Integer;
    begin
        for i := 1 to MatrixSize do
        begin
            for j := 1 to MatrixSize - i do
            begin
                if matrix[i,j] > 0 then
                begin
                    matrix[i, j] := 0;
                end;
            end;
        end;
    end;

    procedure ReplaceOnOne(var matrix: MatrixSquare);
    var
        i, j: Integer;
    begin
        for i := 1 to MatrixSize do
        begin
            for j := 1 to MatrixSize - i do
            begin
                if matrix[i, j] >= 0 then
                begin
                    matrix[i, j] := 1;
                end
                else
                begin
                    matrix[i, j] := -1;
                end;
            end;
        end;
    end;

end.

