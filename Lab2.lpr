program Lab2;
uses
    MatrixManager;
var
    matrix: MatrixSquare;
    countNegative, different: Integer;
begin
    Randomize;
    FillMatrix(matrix);
    WriteLn('First matrix: ');
    PrintMatrix(matrix);
    countNegative := GetCountNegativeElement(matrix);
    WriteLn('Count negative elements: ', countNegative);
    WriteLn;
    ReplaceOnZero(matrix);
    WriteLn('Second matrix: ');
    PrintMatrix(matrix);
    different := GetMinMaxDifference(matrix);
    Write('Min Max Different: ', different);
    WriteLn;
    ReplaceOnOne(matrix);
    WriteLn('Third matrix: ');
    PrintMatrix(matrix);
    ReadLn;
end.

